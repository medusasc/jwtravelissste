// @ts-ignore
import colors from 'vuetify/es5/util/colors';
import * as routes from './routes.json';

const ENVIRONMENT: any = {
  offline: 'off',
  development: 'dev',
  production: 'prod'
};

const ENV = `${process.env.NODE_ENV}`;


export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - jwtravelissste-web',
    title: 'jwtravelissste-web',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
        { src: 'https://cdn.jsdelivr.net/npm/sweetalert2@10' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],
  styleResources: {
      scss: [
          '@/assets/css/palette.scss',
          '@/assets/css/variables.scss',
          '@/assets/css/global.scss'
      ]
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
      '@/plugins/utils',
  ],
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxt/typescript-build',
    '@nuxtjs/dotenv',
    '@nuxtjs/vuetify'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/style-resources',
    'nuxt-vuex-localstorage',
  ],
  dotenv: {
      filename: `.env.${ENVIRONMENT[ENV]}`
  },

  generate: {
      routes () {
          const routesId = process.argv.indexOf( '-r' );
          if ( routesId && routesId >= 0 ) {
              const routesList = process.argv[routesId + 1];
              return routesList ? routesList.split( ',' ) : [];
          }
          return routes.routes;
      },
      dir: './dist'
  },

  module: {
    rules: [
        {
            test: /\.ts$/,
            exclude: /node_modules/,
            loader: 'ts-loader'
        },
        // {
        //     test: /\.(sass|scss)$/,
        //     use: [
        //         'vue-style-loader',
        //         'css-modules-typescript-loader',
        //         {
        //             loader: 'css-loader',
        //             options: {
        //                 modules: true
        //             }
        //         },
        //         {
        //             loader: 'sass-loader',
        //             options: {
        //                 indentedSyntax: true
        //             }
        //         }
        //     ]
        // }
    ]
  },
  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/css/variables.scss'],
    treeShake: true,
    defaultAssets: {
        font: {
            family: 'Open Sans'
        },
        icons: 'mdi'
    },
    theme: {
      themes: {
        light: {
            primary: colors.blue.darken2,
            accent: colors.grey.darken3,
            secondary: colors.amber.darken3,
            info: colors.teal.lighten1,
            warning: colors.amber.base,
            error: colors.deepOrange.accent4,
            success: colors.green.accent3
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    filenames: {
        css: ( { isDev }:any ) => isDev ? '[name].css' : '[contenthash].css'
    },
    babel:{
      plugins: [
        ['@babel/plugin-proposal-private-methods', { loose: true }]
      ]
    },
    extractCSS: {
        ignoreOrder: true
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                styles: {
                    name: 'styles',
                    test: /\.(scss|vue)$/,
                    chunks: 'all',
                    enforce: true
                }
            }
        }
    },
    publicPath: '/static',

    extend ( config: any, { isDev, isClient }: { isDev: boolean, isClient: boolean } ) {
      if ( isDev && isClient && process.env.ESLINT ) {
          config.module.rules.push( {
              enforce: 'pre',
              test: /\.(js|vue)$/,
              loader: 'eslint-loader',
              exclude: /(node_modules)|(commons)|(\.nuxt)|(\.svg$)/
          } );
      }
  }
  }
}
