export const state = () => ( {
  currentUser: null,
  currentProfile: null,
  userType: null
} );

export const mutations = {
  currentProfile ( state:any, value: any | null ) {
      state.currentProfile = value;
  },
  userType ( state:any, value: any | null ) {
      state.userType = value;
  }
};
