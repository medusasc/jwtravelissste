export const ratingIcons: any = {
  1: 'mdi-eye',
  2: 'mdi-pencil',
  3: 'mdi-delete',
  NONE: 'mdi-all-inclusive'
};
