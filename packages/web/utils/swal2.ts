export const displayAlert = ( type: 'warning'|'error'|'info'|'success', text: string, timer: number ) => {
  // @ts-ignore
  Swal.fire( {
      toast: true,
      text: `${text}`,
      position: 'bottom-start',
      showConfirmButton: false,
      showCloseButton: true,
      timer,
      timerProgressBar: true,
      customClass: {
          popup: `sovtur-popup sovtur-popup-${type}`,
          icon: 'sovtur-icon',
          cancelButton: 'sovtur-cancelButton',
          content: 'sovtur-content',
          container: 'sovtur-container',
          header: 'sovtur-header'
      }
  } );
};

export const displayPopUp = ( icon: 'warning'|'error'|'info'|'success', text: string, confirmButtonText: string ) => {
  // @ts-ignore
  Swal.fire( {
      text: `${text}`,
      icon: `${icon}`,
      showConfirmButton: true,
      confirmButtonText,
      showCloseButton: true,
      // imageUrl:`${imageUrl}`,
      imageHeight: 200,
      imageAlt: 'A tall image',
      customClass: {
          popup: `sovtur-displayPopup sovtur-displayPopup`,
          cancelButton: 'sovtur-displayPopup'
      }
  } );
};
