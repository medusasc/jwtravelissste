import Vue from 'vue';
import { displayAlert, displayPopUp } from '@/utils/swal2';

const UtilsPlugin = {
    install ( Vue: any ) {
        Vue.prototype.$Swal = displayAlert;
        Vue.prototype.$SwalPopUp = displayPopUp;
    }
};

export default () => {
    Vue.use( UtilsPlugin );
};
