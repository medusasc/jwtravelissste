import Vue from 'vue';
declare module 'vue/types/vue' {
    interface Vue {
        $Swal: any;
        $t: any;
        $vuetify: any;
        $SwalPopUp: any;
    }

declare const window: any;
