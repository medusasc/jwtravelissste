interface CssExports {
    'fontStyle': string;
    'sovtur-popup': string;
    'sovtur-cancelButton': string;
    'sovtur-popup-error': string;
    'sovtur-popup-info': string;
    'sovtur-popup-success': string;
    'sovtur-popup-warning': string;
    'swal2-confirm': string;
    'swal2-content': string;
    'swal2-styled': string;
    'toastError': string;
    'toastInfo': string;
    'toastSuccess': string;
    'toastWarning': string;
}
export const cssExports: CssExports;
export default cssExports;
